<?php namespace Wwt\Blog;

use Hummingbird\Classes\PluginBase;

/**
 * Plugin class for handling plugin specific functions
 *
 * @package WWT\Blog
 * @author Daryl Phillips
 *
 */

class Plugin extends PluginBase {
	public function __construct($package_details = array()) {
		$this->package = $package_details;
	}

    /**
     * Return the package name (if available)
     * 
     * @return String
     */
    public function getPackageName() {
        return "wwtblog";
    }
}