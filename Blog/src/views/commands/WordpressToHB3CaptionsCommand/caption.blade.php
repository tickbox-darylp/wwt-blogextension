<figure>
    @if( !empty($image) )
        {!! $image !!}
    @endif

    @if( !empty($caption) ) 
        <figcaption>{!! trim($caption) !!}</figcaption>
    @endif
</figure>