<div class="row">
    {!! Form::open(array('route' => array('hummingbird.blog.index'), 'method' => 'get'), array('class' => 'form-inline')) !!}
        <div class="col-md-3">
            <h6>Search:</h6>
            {!! Form::text('s', Request::get('s'), array('class' => 'form-control')) !!}
        </div>

        <div class="col-md-3">
            <h6>By Category:</h6>

            <select name="category" class="form-control">
                <option value="">--- Please select ---</option>
                <option value="uncategorised" @if( Request::get('category') == 'uncategorised') selected="selected" @endif>Uncategorised</option>
                @if( App::make('plugins.Tickbox.Blog')->getBaseTaxonomyCategory() )
                    @foreach(App::make('plugins.Tickbox.Blog')->getBaseTaxonomyCategory()->children()->objectPermissions()->orderBy('name')->get() as $ChildCategory)
                        @include('wwt/wwtblog::admin.partials.filter-category-options', array('counter' => 1))
                    @endforeach
                @endif
            </select>
        </div>

        <div class="col-md-3">
            <h6>By status:</h6>
            {!! Form::select('status', [NULL => '--- All ---', 'draft' => 'Draft', 'public' => 'Public'], Request::get('status'), array('class' => 'form-control')) !!}
        </div>

        <div class="col-md-3">
            <h6>&nbsp;</h6>
            <button type="submit" class="btn btn-default filter-results">Filter results</button>
            <a href="{!! route('hummingbird.blog.index') !!}" class="btn">Reset search</a>
        </div>
    {!! Form::close() !!}
</div>