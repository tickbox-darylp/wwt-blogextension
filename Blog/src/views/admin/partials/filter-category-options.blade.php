<option value="{!! $ChildCategory->id !!}" @if(Request::get('category') == $ChildCategory->id) selected="selected" @endif>{!! str_repeat("-", $counter) !!} {!! $ChildCategory->name !!}</option>

@foreach( $ChildCategory->children()->objectPermissions()->orderBy('name')->get() as $SubChildCategory )
    @include('wwt/wwtblog::admin.partials.filter-category-options', array('ChildCategory' => $SubChildCategory, 'counter' => $counter + 1))
@endforeach