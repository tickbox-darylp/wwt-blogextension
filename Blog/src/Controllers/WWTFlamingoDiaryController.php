<?php namespace Wwt\Blog\Controllers;

use ArrayHelper;
use Breadcrumbs;
use General;
use Input;
use Log;
use Redirect;
use View;
use Hummingbird\Controllers\FrontendController;
use Hummingbird\Traits\ShortcodeModuleTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Tickbox\Blog\Models\Post;
use Tickbox\Blog\Models\BlogTag;
use Tickbox\Blog\Models\BlogCategory;
use Tickbox\Blog\Traits\BlogViewTrait;

use Wwt\Centres\Models\Centre;

/**
 * 
 *
 * @author  Daryl Phillips <darylp@tickboxmarketing.co.uk>
 * @version 1.0.0
 * @package Wwt\Blog
 */
class WWTFlamingoDiaryController extends FrontendController {
    use ShortcodeModuleTrait, BlogViewTrait;
    
    public $pagination          = 15;
    public $handles_children    = false;
    public $renderPartial       = false;
    public $renderPartialPath   = 'theme::plugins.blog.wwt.custom-blog-listings';
    protected $BlogShortcode    = '[HB::BLOG]';
    protected $BaseBlogListingsRouteName = 'WWT.BlogFlamingoDiaryArticle.View';
    protected $BaseBlogArticleRouteName  = 'WWT.BlogFlamingoDiary.View';
    
    /**
     * Main constructor for initialising CMS features
     */
    public function __construct( Request $request ) {
        $this->handles_children = count( $request->segments() ) > 4;

        parent::__construct( $request );

        $this->data['CustomBlogPath'] = '/wetland-centres/slimbridge/diaries/flamingo-diary/';

        $this->hasCentre();

        $this->middleware(function ($request, $next) {
            $response = $this->hasPageFilter();

            if( !empty($response) && get_class( $response ) == Response::class ) {
                return $response;
            }

            return $next($request);
        });

        $this->scanContentRenderPartial();
    }

    /**
     * Check that the permalink provided is actually a valid centre
     * Throw error if not
     */
    public function hasCentre() {
        try {
            if( class_exists('Wwt\Centres\Models\Centre') ):
                $this->data['Centre'] = Centre::where('default_url', "/wetland-centres/slimbridge/")->firstOrFail();
            endif;
        }
        catch(\Exception $e) {
            // Not found - error
            return parent::error();
        }
    }

    /**
     * Index page for showing news on a centre specific page
     * 
     * @return Response
     */
    public function index() {
        $this->data['posts'] = Post::search($this->request->get('s'))->live()
            ->future()
            ->byTaxonomy($this->data['page']->categories->pluck('id')->all())
            ->presentBy('post_date', 'DESC')
            ->paginate($this->pagination);
        
        if( $this->renderPartial ) {
            $this->data['page']->content = str_replace($this->BlogShortcode, View::make( $this->renderPartialPath )->with( $this->data )->render(), $this->data['page']->content);
            return parent::view();
        }
        
        return parent::view('theme::plugins.blog.wwt.custom-blog-index', NULL);
    }

    public function viewArticle($centre_url, $slug = NULL) {
        try {
            $this->taxonomyItems            = $this->data['page']->categories->pluck('id')->all();
            $this->data['post']             = $this->getPost();
            $this->data['post']->content    = $this->render_shortcodes_modules( $this->data['post']->content );
            $this->data['BlogPrevious']     = Post::live()->future()->byTaxonomy( $this->getTaxonomyItems() )->previous($this->data['post']->id)->first();
            $this->data['BlogNext']         = Post::live()->future()->byTaxonomy( $this->getTaxonomyItems() )->next($this->data['post']->id)->first();

            $this->registerSEO( $this->data['post'] );

            $BlogUrl = parse_url( generate_blog_url( $this->data['post'] ) );
            
            Breadcrumbs::for("{$this->data['page']->breadcrumbPath}.{$this->data['post']->permalink}", function ($breadcrumbs) use($BlogUrl) {

                $breadcrumbs->parent($this->data['page']->breadcrumbPath);
                $breadcrumbs->push($this->data['post']->title, $BlogUrl['path']);
            });

            return parent::view('theme::plugins.blog.wwt.custom-blog-view', NULL);
        }
        catch(\Exception $e) {
            Log::error("Flamingo diaries post not found: {$this->request->fullUrl()}.");
        }
        
        // Not found - error
        return parent::error();
    }


    /**
     * 
     * Get all posts by category
     * @param  String $slug
     * @return Void
     */
    public function viewByCategory($slug = NULL) {
        try {
            $taxonomy = $this->data['page']->categories()->pluck('id')->all();

            $blogCat = BlogCategory::where('slug', 'blog')->first();

            if (!$taxonomy && $blogCat) {
                $taxonomy[] = $blogCat->id;
            }

            $TaxonomyCategories = ArrayHelper::cleanExplodedArray( explode("+", $slug));
            $CleanTaxonomyCategories = BlogCategory::whereIn('slug', $TaxonomyCategories)->get();
            $TaxonomyItems = array_merge($taxonomy, $CleanTaxonomyCategories->pluck('id')->all());

            if( $CleanTaxonomyCategories->count() <= 0 ) {
                throw new \Exception("No categories found");
            }  

            if( $CleanTaxonomyCategories->count() == 1 ) {
                // To do pulling out wrong category
                $this->data['TAXONOMY_PAGE'] = $CleanTaxonomyCategories->first();
            }

            $this->data['posts'] = Post::search($this->request->get('s'))->future()
                                       ->live()
                                       ->byTaxonomy( $TaxonomyItems, true )
                                       ->presentBy('post_date', 'DESC')
                                       ->paginate($this->pagination);

            return parent::view('theme::plugins.blog.wwt.custom-blog-index', NULL);
        }
        catch(\Exception $e) {
            Log::error($e->getMessage());
        }

        return parent::error();
    }


    /**
     * Get all posts by tag
     * @param  String $slug
     * @return Void
     */
    public function viewByTag($slug = NULL) {
        try {
            $taxonomy = $this->data['page']->categories()->pluck('id')->all();

            $TaxonomyTags = ArrayHelper::cleanExplodedArray( explode("+", $this->getRouteParameter('slug')));
            $CleanTaxonomyTags = BlogTag::whereIn('slug', $TaxonomyTags)->get();
            $TaxonomyItems = array_merge($taxonomy, $CleanTaxonomyTags->pluck('id')->all());

            if( $CleanTaxonomyTags->count() <= 0 ) {
                throw new \Exception("No tags found");
            }

            if( $CleanTaxonomyTags->count() == 1 ) {
                $this->data['TAXONOMY_PAGE'] = $CleanTaxonomyTags->first();
            }
            
            $this->data['Taxonomy']['tags'] = $CleanTaxonomyTags->pluck('slug')->all();
            $this->data['Taxonomy']['categories'] = $this->data['page']->categories()->pluck('slug')->all();

            $this->data['posts'] = Post::search($this->request->get('s'))->future()
                                       ->live()
                                       ->byTaxonomy( $TaxonomyItems, true )
                                       ->presentBy('post_date', 'DESC')
                                       ->paginate($this->pagination);

            if( $this->renderPartial ) {
                $this->data['page']->content = str_replace($this->BlogShortcode, View::make( $this->renderPartialPath )->with( $this->data )->render(), $this->data['page']->content);
                return parent::view();
            }

            return parent::view('theme::plugins.blog.wwt.custom-blog-index', NULL);
        }
        catch(\Exception $e) {
            Log::error($e->getMessage());
        }

        return parent::error();
    }
}
