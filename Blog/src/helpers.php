<?php 

if( !function_exists('custom_generate_blog_url') ) {
    /**
     * Custom version of the generate_blog_url() found in Tickbox\Blog\Helpers
     * TODO: Translate this into an application hook
     * 
     * @param  Tickbox\Blog\Models\Post $BlogPost
     * @return String
     */
    function custom_generate_blog_url($BlogPost) {
        $BlogURL = wwt_get_blog_url_from_post($BlogPost);

        return ($BlogURL) ? "{$BlogURL}" : route('Blog.View', [$BlogPost->permalink]);
    }
}

if( !function_exists('wwt_get_blog_route_from_post_categories') ) {
    function wwt_get_blog_route_from_post_categories($Post, $Categories = []) {
        $ActiveCentre         = ( class_exists('Wwt\Centres\Models\Centre') && App::bound('wwt.alerts') ) ? WWTCentreAlerts::getCentre() : NULL;

        if( in_array('Blog - Waterlife online', $Categories) ):
            return 'WWT.WaterLifeArticle';
        endif;

        if( in_array('Blog - duck', $Categories) ):
            return 'WWT.BlogDuckDiaryArticle';
        endif;

        if( in_array('Blog - flamingo', $Categories) ):
            return 'WWT.BlogFlamingoDiaryArticle';
        endif;

        if( in_array('Blog - Bewick\'s Swan', $Categories) ):
            return 'WWT.BlogBewickSwanDiaryArticle';
        endif;

        // Search for centre specific blogs
        if( count($Categories) > 0 ) {
            foreach( $Post->categories as $Category ) {
                if( $ActiveCentre ) {
                    if( strpos($Category->name, 'Blog -') !== FALSE || strpos($Category->name, 'News -') !== FALSE ) {
                        $Centre     = str_slug(str_replace(['Blog -', 'News -'], '', $Category->name));

                        if( in_array($Centre, ['arundel', 'caerlaverock', 'castle-espie', 'llanelli', 'london', 'martin-mere', 'slimbridge', 'washington', 'welney', 'steart']) ) {
                            return 'WWT.BlogArticle';
                        }
                    }

                    if( strpos($Category->name, 'Sightings -') !== FALSE ) {
                        return 'WWT.Blog.SightingsArticle';
                    }
                }

                if( strpos($Category->name, 'Press Releases') !== FALSE ) {
                    return 'WWT.PressReleaseArticle';
                }
            }
        }

        return 'Blog';
    }
}

if (!function_exists('wwt_get_blog_url_from_post')) {
    /**
     * Generate the blog URL based on the Post Category
     * @param  BlogPost $Post [description]
     * @return String
     */
    function wwt_get_blog_url_from_post($Post) {
        $ActiveCentre         = ( class_exists('Wwt\Centres\Models\Centre') && App::bound('wwt.alerts') ) ? WWTCentreAlerts::getCentre() : NULL;
        $Categories = $Post->categories->pluck('name')->all();

        $BlogRoute = wwt_get_blog_route_from_post_categories($Post, $Categories);

        // Search for centre specific blogs
        if( count($Categories) > 0 ) {
            foreach( $Post->categories as $Category ) {
                if( $ActiveCentre ) {
                    if( strpos($Category->name, 'Blog -') !== FALSE || strpos($Category->name, 'News -') !== FALSE ) {
                        $Centre     = str_slug(str_replace(['Blog -', 'News -'], '', $Category->name));

                        if( str_slug(trim(str_replace(["WWT", "Marshes"], "", $ActiveCentre->name))) == $Centre ) {
                            return ( $Centre == 'steart') ? route("{$BlogRoute}.View", array('steart-marshes', $Post->permalink), false) : route("{$BlogRoute}.View", array($Centre, $Post->permalink), false);
                        }

                        if( in_array($Centre, ['arundel', 'caerlaverock', 'castle-espie', 'llanelli', 'london', 'martin-mere', 'slimbridge', 'washington', 'welney']) ) {
                            return ( $Centre == 'steart') ? route("{$BlogRoute}.View", array('steart-marshes', $Post->permalink), false) : route("{$BlogRoute}.View", array($Centre, $Post->permalink), false);
                        }
                    }

                    if(strpos($Category->name, 'Sightings -') !== FALSE) {
                        $Centre     = str_slug(str_replace('Sightings -', '', $Category->name));
                        return route("{$BlogRoute}.View", array($Centre, $Post->permalink), false);
                    }
                }

                if(strpos($Category->name, 'Press Releases') !== FALSE) {
                    return route("{$BlogRoute}.View", array($Post->permalink), false);
                }
            }
        }

        return route("{$BlogRoute}.View", $Post->permalink, false);
    }
}

if( !function_exists('generate_blog_url_from_post_taxonomy') ) {
    /**
     * Get the blog URL from the post category(ies)
     */
    function generate_blog_url_from_post_taxonomy($post) {
        $CentreURL = wwt_get_centre_url_from_post_categories( $post );
        $BlogRoute = wwt_get_blog_route_from_post_categories( $post, $post->categories->pluck('name')->all() );

        if( in_array($BlogRoute, ['WWT.BlogArticle', 'WWT.Blog.SightingsArticle']) ):
            if( !empty($CentreURL) ):
                return route(str_replace("Article", "", $BlogRoute) . ".Category.Index", [ $CentreURL, implode("+", $post->categories()->pluck('slug')->all()) ]);
            endif;
        endif;

        $BlogRoute = str_replace("Article", "", $BlogRoute);

        return route("{$BlogRoute}.Category.Index", implode("+", $post->categories()->pluck('slug')->all()));
    }
}

if( !function_exists('get_latest_blog_posts') ) {
    /**
     * Retrieve the latest blog posts, by taxonomy and not including current post
     */
    function get_latest_blog_posts($post, $limit = 3) {
        $tax = $post->taxonomy();

        return Tickbox\Blog\Models\Post::live()->future()->byTaxonomy( $tax['category'] )->whereNotIn('id', [$post->id])->presentBy('post_date', 'DESC')->take( $limit )->get();
    }
}


if( !function_exists('wwt_get_centre_url_from_post_categories') ) {
    /**
     * If the post is within Blog - {centre} or News - {Centre},
     * then return the correct centre URL
     * 
     * @param Tickbox\Blog\Models\Post $Post
     * @return Mixed
     */
    function wwt_get_centre_url_from_post_categories($Post) {
        $ActiveCentre         = ( class_exists('Wwt\Centres\Models\Centre') && App::bound('wwt.alerts') ) ? WWTCentreAlerts::getCentre() : NULL;

        if( $Post->categories->count() > 0 && $ActiveCentre ) {
            foreach( $Post->categories as $Category ) {
                if(strpos($Category->name, 'Blog -') !== FALSE || strpos($Category->name, 'News -') !== FALSE || strpos($Category->name, 'Sightings -') !== FALSE ) {
                    $Centre     = str_replace(["blog-", "news-", "sightings-"], "", str_slug($Category->name));

                    if( in_array($Centre, ['arundel', 'caerlaverock', 'castle-espie', 'llanelli', 'london', 'martin-mere', 'slimbridge', 'washington', 'welney', 'steart']) ) {
                        return ($Centre == 'steart') ? 'steart-marshes' : $Centre;
                    }
                }
            }
        }

        return NULL;
    }
}