<?php 
Route::group(array('prefix' => 'discover-wetlands/wetland-wildlife/swans'),  function() {
    Route::get('swan-news', ['as' => 'WWT.BlogSwans.View', 'uses' => 'Wwt\Blog\Controllers\WWTSwanNewsController@index']);
    Route::get('swan-news/tag/{slug}', array('as' => 'WWT.BlogSwans.Tag.Index', 'uses' => 'Wwt\Blog\Controllers\WWTSwanNewsController@viewByTag'));
    Route::get('swan-news/{slug?}', ['middleware' => ['Tickbox.Blog.checkLegacyBlogPermalinkSetup'], 'as' => 'WWT.BlogSwansArticle.View', 'uses' => 'Wwt\Blog\Controllers\WWTSwanNewsController@viewArticle'])->where('slug', '(.*)?');
});