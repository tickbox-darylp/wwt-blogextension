<?php 
Route::group(array('prefix' => 'news-and-stories'),  function() {
    Route::get('blog', ['as' => 'WWT.WaterLife.View', 'uses' => 'Wwt\Blog\Controllers\WWTWaterLifeBlogController@index']);
    Route::get('blog/category/{slug?}', array('as' => 'WWT.WaterLife.Category.Index', 'uses' => 'Wwt\Blog\Controllers\WWTWaterLifeBlogController@viewByCategory'))->where('slug', '(.*)?');
    Route::get('blog/tag/{slug?}', array('as' => 'WWT.WaterLife.Tag.Index', 'uses' => 'Wwt\Blog\Controllers\WWTWaterLifeBlogController@viewByTag'))->where('slug', '(.*)?');
    Route::get('blog/{slug?}', ['middleware' => ['Tickbox.Blog.checkLegacyBlogPermalinkSetup'], 'as' => 'WWT.WaterLifeArticle.View', 'uses' => 'Wwt\Blog\Controllers\WWTWaterLifeBlogController@viewArticle'])->where('slug', '(.*)?');
});