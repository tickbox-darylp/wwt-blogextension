<?php 
Route::group(array('prefix' => 'wetland-centres/slimbridge/diaries'),  function() {
    Route::get('bewicks-swan-diary', ['as' => 'WWT.BlogBewickSwanDiary.View', 'uses' => 'Wwt\Blog\Controllers\WWTBlogBewicksSwanController@index']);
    Route::get('bewicks-swan-diary/category/{slug?}', array('as' => 'WWT.BlogBewickSwanDiary.Category.Index', 'uses' => 'Wwt\Blog\Controllers\WWTBlogBewicksSwanController@viewByCategory'))->where('slug', '(.*)?');
    Route::get('bewicks-swan-diary/tag/{slug?}', array('as' => 'WWT.BlogBewickSwanDiary.Tag.Index', 'uses' => 'Wwt\Blog\Controllers\WWTBlogBewicksSwanController@viewByTag'))->where('slug', '(.*)?');
    Route::get('bewicks-swan-diary/{slug?}', ['middleware' => ['Tickbox.Blog.checkLegacyBlogPermalinkSetup'], 'as' => 'WWT.BlogBewickSwanDiaryArticle.View', 'uses' => 'Wwt\Blog\Controllers\WWTBlogBewicksSwanController@viewArticle'])->where('slug', '(.*)?');

    Route::get('phoebes-duck-diary', ['as' => 'WWT.PhoebesDuckDiary.View', 'uses' => 'Wwt\Blog\Controllers\WWTDuckDiaryController@phoebes_index']);
    Route::get('phoebes-duck-diary/category/{slug?}', array('as' => 'WWT.PhoebesDuckDiary.Category.Index', 'uses' => 'Wwt\Blog\Controllers\WWTDuckDiaryController@viewByCategory'))->where('slug', '(.*)?');
    Route::get('phoebes-duck-diary/tag/{slug?}', array('as' => 'WWT.PhoebesDuckDiary.Tag.Index', 'uses' => 'Wwt\Blog\Controllers\WWTDuckDiaryController@viewByTag'))->where('slug', '(.*)?');
    Route::get('phoebes-duck-diary/{slug?}', ['middleware' => ['Tickbox.Blog.checkLegacyBlogPermalinkSetup'], 'as' => 'WWT.PhoebesDuckDiaryArticle.View', 'uses' => 'Wwt\Blog\Controllers\WWTDuckDiaryController@phoebes_viewArticle'])->where('slug', '(.*)?');

    Route::get('flamingo-diary', ['as' => 'WWT.BlogFlamingoDiary.View', 'uses' => 'Wwt\Blog\Controllers\WWTFlamingoDiaryController@index']);
    Route::get('flamingo-diary/category/{slug?}', array('as' => 'WWT.BlogFlamingoDiary.Category.Index', 'uses' => 'Wwt\Blog\Controllers\WWTFlamingoDiaryController@viewByCategory'))->where('slug', '(.*)?');
    Route::get('flamingo-diary/tag/{slug?}', array('as' => 'WWT.BlogFlamingoDiary.Tag.Index', 'uses' => 'Wwt\Blog\Controllers\WWTFlamingoDiaryController@viewByTag'))->where('slug', '(.*)?');
    Route::get('flamingo-diary/{slug?}', ['middleware' => ['Tickbox.Blog.checkLegacyBlogPermalinkSetup'], 'as' => 'WWT.BlogFlamingoDiaryArticle.View', 'uses' => 'Wwt\Blog\Controllers\WWTFlamingoDiaryController@viewArticle'])->where('slug', '(.*)?');

    Route::get('duck-diary', ['as' => 'WWT.BlogDuckDiary.View', 'uses' => 'Wwt\Blog\Controllers\WWTDuckDiaryController@index']);
    Route::get('duck-diary/category/{slug?}', array('as' => 'WWT.BlogDuckDiary.Category.Index', 'uses' => 'Wwt\Blog\Controllers\WWTDuckDiaryController@viewByCategory'))->where('slug', '(.*)?');
    Route::get('duck-diary/tag/{slug?}', array('as' => 'WWT.BlogDuckDiary.Tag.Index', 'uses' => 'Wwt\Blog\Controllers\WWTDuckDiaryController@viewByTag'))->where('slug', '(.*)?');
    Route::get('duck-diary/{slug?}', ['middleware' => ['Tickbox.Blog.checkLegacyBlogPermalinkSetup'], 'as' => 'WWT.BlogDuckDiaryArticle.View', 'uses' => 'Wwt\Blog\Controllers\WWTDuckDiaryController@viewArticle'])->where('slug', '(.*)?');
});