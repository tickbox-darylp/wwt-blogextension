<?php namespace Wwt\Blog\Commands;

use Input;
use View;

use Illuminate\Console\Command;
use Tickbox\Blog\Models\Post;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class WordpressToHB3CaptionsCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'wwtblog:wp-to-hb3-captions';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Clean old WordPress imported captions and replace with appropriate <figure /> HTML';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire() {
        try {
	        $Posts = Post::where('content', 'LIKE', '%[caption%')->orderBy("id", "ASC")->get();

	        foreach($Posts as $Post) {
	        	$Post->disableVersioning();
	        	$Post->timestamps = false;
	        	$Post->content = $this->cleanContent($Post->content);
		        $Post->save();

		        // Save the versions too
		        if( $Post->versions->count() > 0 ) {
		        	foreach($Post->versions as $PostVersion) {
		        		$PostVersion->timestamps = false;
		        		$PostVersion->content = $this->cleanContent($PostVersion->content);
				        $PostVersion->save();
		        	}
		        }

		        $this->info('We\'ve updated this post: ' . route('Blog.View', [$Post->permalink]));
		    }
        }
        catch(\Exception $e) {
        	$this->error("Error with Post {$Post->id}: " . $e->getTraceAsString());
        }
	}

	/**
	 * Clean the content of all captions
	 * @return String $content
	 */
	protected function cleanContent($content) {
	    preg_match_all("/\[caption.*\](.*?)\[\/caption\]/", $content, $matches);

	    if( !empty($matches) && count($matches) > 0 ):
	    	foreach($matches[0] as $caption_key => $caption_match):
	    		preg_match("#(<img.*?>)#", $caption_match, $img_matches);
	    		preg_match('/caption="([^"]*)"/', $caption_match, $caption_tag_matches);

	    		$MainCaption 	= ( count($caption_tag_matches) > 0 && isset($caption_tag_matches[1]) ) ? trim( $caption_tag_matches[1] ) : NULL;
	    		$BackupCaption 	= strip_tags($matches[1][$caption_key]);
	    		$MainCaption 	= ( !empty($MainCaption) ) ? $MainCaption : $BackupCaption;

				$ImageWithoutCaption = View::make('wwt/wwtblog::commands.WordpressToHB3CaptionsCommand.caption')->with([
					'caption' 	=> $MainCaption,
					'image'		=> $img_matches[1]
				])->render();

				$content = str_replace($matches[0][$caption_key], $ImageWithoutCaption, $content);
	    	endforeach;
	    endif;

	    return $content;
	}


	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array();
	}

}
