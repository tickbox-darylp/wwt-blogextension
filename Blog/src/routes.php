<?php 
Route::group(array('middleware' => ['hb3.AfterInstallation', 'web']), function()
{
    include __DIR__ . '/routes/centre-news.php';
    include __DIR__ . '/routes/centre-slimbridge-diaries.php';
    include __DIR__ . '/routes/discover-wetlands.php';
    include __DIR__ . '/routes/latest-sightings.php';
    include __DIR__ . '/routes/swan-news.php';
    include __DIR__ . '/routes/press-releases.php';
});
