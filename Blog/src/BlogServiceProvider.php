<?php namespace Wwt\Blog;

use App;
use Hummingbird\PluginServiceProvider;
use Hummingbird\Interfaces\PluginServiceProviderInterface;

use Wwt\Blog\Commands\WordpressToHB3CaptionsCommand;

class BlogServiceProvider extends PluginServiceProvider implements PluginServiceProviderInterface {
    protected $files        = [
        __DIR__ . '/routes.php',
        __DIR__ . '/helpers.php'
    ];


    /**
     * Set the plugin name
     *
     * @return void
     */
    public function setPluginName() {
        $this->pluginName = 'plugins.WWT-Blog';
    }


    /**
     * Set the package name
     *
     * @return void
     */
    public function setPackageName() {
        $this->packageName = 'wwt/wwtblog';
    }


    /**
     * Set the directory for our views
     *
     * @return void
     */
    public function setViewDirectory() {
        $this->views = __DIR__ . '/views';
    }


    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
        
        $this->commands(WordpressToHB3CaptionsCommand::class);
    }


    /**
     * Registering the main Plugin file used for Plugin Services
     *
     * @return void
     */
    public function registerPlugin() {
        $this->app->bind($this->pluginName, function () {
            return new Plugin($this->PluginManager->getPackageInformation('Wwt', 'Wwtblog'));
        });
    }
}
