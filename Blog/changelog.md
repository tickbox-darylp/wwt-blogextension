# WWT - Blog (Extension)
WWT Specific functions/actions for extending the standard Blogging platform

# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.4.1] - 2025-01-31
### Fixed
- WWT-26510179: Error 500 pages being thrown on invalid blog articles


## [2.4.0] - 2025-01-13
### Updated
- WWT-26381421: HB3 compatibility upgrades to support Laravel 10


## [2.3.2] - 2024-12-12
### Updated
- WWT-26381421: HB3 compatibility upgrades to support Laravel 7


## [2.3.1] - 2024-12-03
### Updated
- WWT-26381421: HB3 compatibility upgrades to support Laravel 5.6

### Fixed
- WWT-26381421: Middleware groups configured incorrectly


## [2.3.0] - 2024-11-22
### Updated
- HB3-26381421: HB3 compatibility upgrades to support Laravel 5.4


## [2.2.0] - 2024-11-14
### Updated
- HB3-26381421: HB3 compatibility upgrades to support Laravel 5.3


## [2.1.0] - 2024-11-07
### Updated
- HB3-26381421: HB3 compatibility upgrades to support 5.2


## [2.0.0] - 2024-08-20
### Updated
- HB3-26381421: HB3 compatibility upgrades to support v2 of CMS (and Laravel 5.0)


## [1.4.1] - 2024-08-09
### Fixed
- WWT-26378765: Breadcrumbs issue with Duck Diaries


## [1.4.0] - 2024-06-04
### Added
- WWT-26171741: Breadcrumbs added to when posts are viewable


## [1.3.2] - 2023-09-18
### Updated
- WWT-26068463: Fixed helper that allows us to get the correct URL for blog "feeds"


## [1.3.1] - 2023-06-09
### Updated
- WWT-25959100: Allowing posts to feed correctly to the landing pages of National News or centre news depending on the location of the feed and the taxonomy assigned


## [1.3.0] - 2023-05-10
### Updated
- WWT-25587782: Namespacing updated for HB3 compatibility


## [1.2.6] - 2022-10-10
### Updated
- TBX-25725271: Amended blog category generation for posts, which are in the primary news section and filtered, for Centre news pages


## [1.2.5] - 2022-08-05
### Updated
- TBX-25655642: Amended blog category generation function where it wouldn't generate the correct Centre URL for viewing blog category projects


## [1.2.4] - 2022-05-27
### Added
- TBX-13401407: New filters to allow for the status of a post and whether it's uncategorised


## [1.2.3] - 2022-01-29
### Added
- TBX-25361981: New blog area for all Press Releases

### Fixed
- TBX-25143554: WWT tag URLs not generating correctly


## [1.2.2] - 2021-06-23
### Added
- New helper function to generate the links for custom and standard blog entries

### Updated
- Existing blog helper to make it more universal across other elements of the site


## [1.2.1] - 2021-06-23
### Added
- New helper function to generate the links for custom and standard blog entries

### Updated
- Existing blog helper to make it more universal across other elements of the site


## [1.2.0] - 2021-02-18
### Added
- Command to help clean out WordPress captions and replace with the HTML5 equivalent
- New helper for generating blog links for custom blog entries

### Updated
- All bespoke blogs to now use the correct permalink structure and redirect if necessary
- Abilities to preview posts based on version number and/or latest key (as per core blog)
- Bespoke blog routes to match the new work provided by client

### Fixed
- Website redirects not working within bespoke blogs
- Incorrect blog URL generation for custom blogs


## [1.1.1] - 2020-11-23
### Added
- Category/Tag based URLs to all custom blogs to allow for customisable URLs


## [1.1.0] - 2020-09-14
### Added
- Brand new SEO facilities following the HB3 upgrade to v1.1.16
- New fields (Facebook/Twitter) meta data now available
- Code quality improved for readability and maintainability

### Removed
- Meta keywords removed from system


## [1.0.5] - 2020-08-10
### Added
- Shortcode ([HB::BLOG]) to listing pages to allow page content to be output


## [1.0.4] - 2020-05-01
### Added
- Allow all custom blogs to be rendered with their shortcode/module counterparts